FROM  alpine:3.16 as build

RUN apk add --no-cache      \
        build-base                                \   
        gnupg                                     \
        pcre-dev                                  \
        wget                                      \
        libressl-dev                                                                    

ARG VERSION=1.14.2               

RUN set -x                                                                      &&  \
    cd /tmp                                                                     &&  \
    gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys                             \
        520A9993A1C052F8                                                        &&  \
    wget -q http://nginx.org/download/nginx-${VERSION}.tar.gz                   &&  \
    wget -q http://nginx.org/download/nginx-${VERSION}.tar.gz.asc               &&  \
    gpg --verify nginx-${VERSION}.tar.gz.asc                                    &&  \
    tar -xf nginx-${VERSION}.tar.gz                                             &&  \
    echo ${VERSION}

WORKDIR /tmp/nginx-${VERSION}

RUN ./configure                                                                     \
        --with-ld-opt="-static"                                                     \
        --with-http_ssl_module                                                      \
        --without-http_gzip_module                                              &&  \                                               
    make install                                                                &&  \
    strip /usr/local/nginx/sbin/nginx

RUN ln -sf /dev/stdout /usr/local/nginx/logs/access.log                         &&  \
    ln -sf /dev/stderr /usr/local/nginx/logs/error.log

FROM scratch

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /usr/local/nginx /usr/local/nginx

EXPOSE 80

ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]
